import pygame #import库  
from pygame.sprite import Group
from  block import Block
from  block import Block_2

class Background():
    def __init__(self,n,screen):
        self.n=n[0]
        self.screen = screen
        self.image = pygame.image.load('images/beijing5.bmp')
        self.image = pygame.transform.scale(self.image,(1000,600))
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 0

    def draw_backpicture(self,n):
        if n[0] == 1:
            self.image = pygame.image.load('images/beijing_first.bmp')
        elif n[0] == 2:
            self.image = pygame.image.load('images/beijing_second.bmp')
        elif n[0] == 3:
            self.image = pygame.image.load('images/beijing_third.bmp')

        self.image = pygame.transform.scale(self.image,(1000,600))
        self.screen.blit(self.image, (self.rect.x,self.rect.y))

    def draw_something_good(self):
        self.image = pygame.image.load('images/beijing3.bmp')
        self.image = pygame.transform.scale(self.image,(1000,600))
        self.screen.blit(self.image, (self.rect.x,self.rect.y))

    def draw_background(self):
        blocks = Group()  #记录block类的数组，用于后面的碰撞检测
        
        if self.n == 1:
            for i in range(35):
                block = Block(20*i ,580,self.screen)
                block.drawblock()
                blocks.add(block)
            for i in range(12):
                block = Block(20*(i+38) ,580,self.screen)
                block.drawblock()
                blocks.add(block)
            for i in range(35):
                block = Block(20*i ,560,self.screen)
                block.drawblock()
                blocks.add(block)
            for i in range(12):
                block = Block(20*(i+38) ,560,self.screen)
                block.drawblock()
                blocks.add(block)
           
            
            block_2 = Block_2(80,480,self.screen)
            block_2.drawblock()
            blocks.add(block_2)
            block_2 = Block_2(200,480,self.screen)
            block_2.drawblock()
            blocks.add(block_2)
            block_2 = Block_2(270,400,self.screen)
            block_2.drawblock()
            blocks.add(block_2)

            block_2 = Block_2(350,320,self.screen)
            block_2.drawblock()
            blocks.add(block_2)
            block_2 = Block_2(410,320,self.screen)
            block_2.drawblock()
            blocks.add(block_2)

            block_2 = Block_2(530,220,self.screen)
            block_2.drawblock()
            blocks.add(block_2)
            block_2 = Block_2(590,220,self.screen)
            block_2.drawblock()
            blocks.add(block_2)

            #block = Block(20*38,540,self.screen)
            #block.drawblock()
            #blocks.add(block)
            #block = Block(20*38,520,self.screen)
            #block.drawblock()
            #blocks.add(block)

            for i in range(5):
                block = Block(20*17 ,540-20*i,self.screen)
                block.drawblock()
                blocks.add(block)
    
            block = Block(20*24,540,self.screen)
            block.drawblock()
            blocks.add(block)
            block = Block(20*24,520,self.screen)
            block.drawblock()
            blocks.add(block)
           
        if self.n == 2:
            for i in range(50):
                block = Block(20*i ,580,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(50):
                block = Block(20*i ,560,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(3):
                block = Block(120+20*i ,540,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(8,50):
                block = Block(90+20*i ,540,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(50):
                block = Block(250+20*i ,520,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(50):
                block = Block(250+20*i ,500,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(4):
                block = Block(300 ,480-20*i,self.screen)
                block.drawblock()
                blocks.add(block)
                
            for i in range(4):
                block = Block(320 ,480-20*i,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(4):
                block = Block(650 ,480-20*i,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(4):
                block = Block(670 ,480-20*i,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(6):
                block = Block(800 +i*20,300,self.screen)
                block.drawblock()
                blocks.add(block)

            
        if self.n == 3:

            for i in range(50):
                block = Block(20*i ,580,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(50):
                block = Block(20*i+40 ,560,self.screen)
                block.drawblock()
                blocks.add(block)
                
            for i in range(30):
                block = Block(20*i+80 ,540,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(28):
                block = Block(20*i+120 ,520,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(26):
                block = Block(20*i+160 ,500,self.screen)
                block.drawblock()
                blocks.add(block)

           
            for i in range(24):
                block = Block(20*i+200 ,480,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(30):
                block = Block(20*i+240 ,460,self.screen)
                block.drawblock()
                blocks.add(block)

            for i in range(2):
                block_2 = Block_2(60*i+850 ,380,self.screen)
                block_2.drawblock()
                blocks.add(block_2)

            for i in range(2):
                block_2 = Block_2(60*i+730 ,300,self.screen)
                block_2.drawblock()
                blocks.add(block_2)

            for i in range(2):
                block_2 = Block_2(60*i+850 ,220,self.screen)
                block_2.drawblock()
                blocks.add(block_2)

            for i in range(14):
                block_2 = Block_2(60*i+10 ,130,self.screen)
                block_2.drawblock()
                blocks.add(block_2)

        return blocks


        
