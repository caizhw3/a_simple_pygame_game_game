import pygame
from pygame.sprite import Sprite

class Bullet(Sprite):
    def __init__(self,width,height,bullet_speed,screen,x,y,direction):
        super().__init__()
        self.screen = screen

        self.width = width
        self.height = height
        #self.image = pygame.transform.scale(self.image,(width,height))
    
        self.direction = direction
        if direction == 1:
            self.image = pygame.image.load('images/bullet2.bmp')
            self.image = pygame.transform.scale(self.image,(self.width,self.height))
        elif direction == 0:
            self.image = pygame.image.load('images/bullet.bmp')
            self.image = pygame.transform.scale(self.image,(self.width,self.height))
            
        self.rect = self.image.get_rect()
        self.rect.centery=y
        self.rect.centerx=x
        self.y=self.rect.y
        self.x=self.rect.x
        self.bullet_speed=bullet_speed

    def update(self):
        if self.direction == 1:  #向左
            self.x -= self.bullet_speed
        else:
            self.x += self.bullet_speed
        self.rect.x=self.x

    def draw_bullet(self):
        self.screen.blit(self.image, (self.rect.x,self.rect.y))
        