import pygame

class Hero():
	
	def __init__(self,ai_settings,screen):
		"""初始化英雄并设置其初始位置"""
		self.screen = screen
		self.ai_settings=ai_settings
		
		self.hp_sum = self.ai_settings.hero_hp_sum
		
		# 移动标志
		self.moving_right = False
		self.moving_left = False
		self.moving_up = False
		self.direction = 0#0 you 1 left

		self.top = False	#是否到达上升的最高点
		self.drop = False	#人物是否下掉状态
		self.stand = True 	#人物有没有站好
		self.p_left = False #人物左端碰撞
		self.p_right = False#人物右端碰撞
		
		self.image = pygame.image.load('images/hero右.bmp')
		self.image = pygame.transform.scale(self.image,(50,50))
		self.rect = self.image.get_rect()
		self.screen_rect = screen.get_rect()
		
		# 将人物放在屏幕底部中央
		self.rect.left = self.screen_rect.left
		self.rect.bottom = self.screen_rect.bottom-40   #初始化站在石块上面
		
		self.center = float(self.rect.centerx)
		self.bottom_ = float(self.rect.bottom)
		
		self.i = self.ai_settings.hero_up_sum
	
	def set_stand(self):
		'''判断是否站稳'''
		self.stand = True
		self.drop = False
		self.top = False
		self.i = self.ai_settings.hero_up_sum
	
	def set_drop(self):
		'''下落状态'''
		self.drop = True
		self.stand = False
	
	def set_p_left(self):
		'''左端碰撞，人物会下落'''
		self.p_left = True
		#self.drop = True
	def set_moving_up_false(self):
		self.moving_up=False	


	def set_p_right(self):
		'''右端碰撞，人物会下落'''
		self.p_right = True
		#self.drop = True
		
	def change_hero(self):
		#self.image_ = pygame.image.load('images/hero右.bmp')
		if self.moving_right:
			self.image = pygame.image.load('images/hero右.bmp')
			self.image = pygame.transform.scale(self.image,(50,50))
			self.direction = 0
		elif self.moving_left:
			self.image = pygame.image.load('images/hero左.bmp')
			self.image = pygame.transform.scale(self.image,(50,50))
			self.direction = 1
		#self.image = self.image_
	
	def update(self):
		"""根据移动标志调整人物的位置"""
		if self.moving_right and self.p_right == False and self.rect.right < self.screen_rect.right:
			self.center += self.ai_settings.hero_lr_speed
		if self.moving_left and self.p_left == False and self.rect.left >0:
			self.center -= self.ai_settings.hero_lr_speed

		if self.moving_up and self.top == False and self.drop==False and self.rect.top >0:
			self.i -= self.ai_settings.hero_up_speed
			self.bottom_ -= self.ai_settings.hero_up_speed
			#print(self.rect.top)
			#print(self.screen.get_rect().top)
			self.stand = False
			if(self.i <= 0) or self.rect.top<=self.screen.get_rect().top+2:

				self.top = True
				self.drop = True
				self.moving_up = False
			else:
				self.drop = False
				self.top = False
		if self.drop and self.stand == False and self.rect.bottom < self.screen_rect.bottom:
			self.bottom_ += self.ai_settings.hero_drop_speed
		
		self.rect.centerx = self.center
		self.rect.bottom = self.bottom_
		
		#可以用来传人物的血条（可以在game_f处通过与子弹碰撞然后减少英雄的生命）
		

			#self.hp_sum -= self.ai_settings.monster_hurt
			
	def blitme(self):
		self.screen.blit(self.image,self.rect)
