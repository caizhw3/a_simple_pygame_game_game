class Settings():
	def __init__(self):
		self.screen_width = 1000
		self.screen_height = 600
		self.bg_color = (100,100,100)
		self.time = 0
		self.time2 = 0
		# 人物血量设置
		self.hero_hp_sum = 100
		self.hero_hp_height = 50
		self.hero_hp_width = 20
		
		# 人物上左右的速度设置
		self.hero_lr_speed = 0.5
		self.hero_up_sum = 120
		self.hero_up_speed = 0.5
		self.hero_drop_speed = 0.5
		
		# 怪物移动速度
		self.monster_lr_speed = 0.05
		self.monster_hurt = 10

		# 子弹设置
		self.bullet_speed_factor = 0.5
		self.bullet_width = 30
		self.bullet_height = 10
		self.hero_bullets_power = 150

		self.monsters_bullets_power = 10
		self.monster_hp = 100

		self.monster_bullets_time = 400
		'''
		
		
		
		self.bullet_color = 60,60,60
		self.bullets_allowed = 10
		self.alien_speed_factor = 1 
		self.fleet_drop_speed = 10
		# fleet_direction为1表示向右移,为-1表示向左移 
		self.fleet_direction = 1
		'''
