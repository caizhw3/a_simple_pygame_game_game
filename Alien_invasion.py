import sys
import pygame
from pygame.sprite import Group
from setting import Settings
from hero import Hero
from scoreboard import Scoreboard
from monster import Monster_little
import game_functions as gf 
from button import Button  
from background import Background
from bullet import Bullet
from netProgramming import netPrograming
from socket import *
from threading import Thread
import BGM
import time


def initbutton(screen): 
	button = []
	danren = Button(screen,"one player",0)
	shuangren = Button(screen,"two player",1)
	button.append(danren)
	button.append(shuangren)
	return button

def one_player():
	#print(1)
	#BGM.music(1)######################################################
	ai_settings = Settings()
	screen = pygame.display.set_mode((ai_settings.screen_width, ai_settings.screen_height))
	n=[1]
	n_temp = 0
	hero = Hero(ai_settings,screen)
	# 创建血量条
	sb = Scoreboard(ai_settings, screen, hero,0)

	bullets = Group()
	monster_bullets = Group()
	monsters = Group()
	waittime = 0 
	while True:
		#time.sleep(0.02)
		if n[0] !=n_temp:
			background = Background(n,screen)
			blocks = background.draw_background()
			gf.create_monster(n,monsters,ai_settings,screen,hero,bullets)
			n_temp=n[0]
					
		if waittime == 0:
			background.draw_backpicture(n)
		waittime+=1
		if waittime == 50:
			waittime=0	
		i = gf.check_events(ai_settings, screen, hero, bullets)
		gf.update_hero(ai_settings,screen,hero,blocks)
		hero.update()
		for monster in monsters:
			monster.update()
		sb.update_scoreboard(ai_settings, screen, hero)
		sb.show_score()
		if n[0] == 4:
			background.draw_something_good()
			pygame.display.flip()
			#time.sleep(2)
			return 
		sb.show_score()
		gf.update_screen(ai_settings, screen, hero, sb, bullets,blocks,n,monsters,monster_bullets)
		if hero.hp_sum <= 0:
			return 
		
def two_player():
	BGM.music(2)###############################################
	ai_settings = Settings()
	screen = pygame.display.set_mode((ai_settings.screen_width, ai_settings.screen_height))
	n=[1]
	n_temp = 0
	hero1 = Hero(ai_settings,screen)
	hero2 = Hero(ai_settings,screen)
	# 创建血量条
	sb1 = Scoreboard(ai_settings, screen, hero1,0)
	sb2 = Scoreboard(ai_settings, screen, hero2,1)

	bullets = Group()
	monster_bullets = Group()
	monsters = Group()
	waittime = 0
	
	##########
	while(np.dest_have_start == False):
		np.sendData()
	##########

	while True:
		#time.sleep(0.02)
		if n[0] !=n_temp:
			background = Background(n,screen)
			blocks = background.draw_background()
			gf.create_monster(n,monsters,ai_settings,screen,hero1,bullets)
			n_temp=n[0]
					
		if waittime == 0:
			background.draw_backpicture(n)
		waittime+=1
		if waittime == 50:
			waittime=0	

		if hero1.hp_sum >0 :
			pg_event = gf.check_events(ai_settings, screen, hero1, bullets)
			gf.update_hero(ai_settings,screen,hero1,blocks)
			hero1.update()
			sb1.update_scoreboard(ai_settings, screen, hero1)
			np.loop_sent_msg(pg_event,hero1) #传递hero  x,y坐标
		else:
			sb1.died()

		if hero2.hp_sum >0 :
			gf.check_events2(ai_settings, screen, hero2, bullets,np)
			#gf.update_hero(ai_settings,screen,hero2,blocks)
			hero2.center = np.dest_hero_centerx
			hero2.bottom_ = np.dest_hero_bottom
			hero2.update()
			sb2.update_scoreboard(ai_settings, screen, hero2)
		else:
			sb2.died()

		for monster in monsters:
			monster.update()
		
		if n[0] >= 4:
			background.draw_something_good()
			pygame.display.flip()
			time.sleep(3)
			return 

		sb1.show_score()
		sb2.show_score()
		if hero1.hp_sum > 0 and hero2.hp_sum > 0:
			gf.update_screen2(ai_settings, screen, hero1, sb1, hero2, sb2,bullets,blocks,n,monsters,monster_bullets)
		elif hero1.hp_sum > 0 and hero2.hp_sum <= 0:
			gf.update_screen(ai_settings, screen, hero1, sb1, bullets,blocks,n,monsters,monster_bullets)
		elif hero1.hp_sum <= 0 and hero2.hp_sum > 0:
			gf.update_screen(ai_settings, screen, hero2, sb2, bullets,blocks,n,monsters,monster_bullets)
		else:
			return 

def run_game():
	pygame.init()
	ai_settings = Settings()
	screen = pygame.display.set_mode(
		(ai_settings.screen_width, ai_settings.screen_height))

	pygame.display.set_caption("MECHA")

	button = initbutton(screen)

	working = [False]
	
	# 开始游戏主循环
	while True:
		while working[0] == False:
			#print(222222)
			model = gf.check_events1(button,working)#0 1 2
			screen.fill((230,230,230))
			button[0].draw_button()
			button[1].draw_button()
			pygame.display.flip()#绘制一个新屏幕 并擦去旧屏幕 以形成平滑移动
			#BGM.shoot_()
		if model == 1:
			one_player()
		elif model == 2:
			two_player()
		working[0] = False
		print(working[0])
		pygame.mouse.set_visible(True)

#destIp = '127.0.0.1'

def multi_thread():
	
	tr = Thread(target = np.recvData)
	#ts = Thread(target = np.sendData)
	tt = Thread(target = run_game)
	
	tr.start()
	#ts.start()
	tt.start()
	
	tr.join()
	#ts.join()
	tt.join()
	#关闭udp通信
	np.udpSocket.close()

#run_game()


destIp = '172.18.32.83'
destIp = input('请输入IP地址：')
destPort = 3411
hostPort = 3411
hostPort = int(input('请输入端口号：'))
destPort = hostPort
np = netPrograming(destIp,destPort,hostPort)
multi_thread()


'''
#run_game

'''
