import pygame
from pygame.sprite import Sprite   #这个模块就是background的基础，block类是地面的砖块，block2是空中的砖块

class Block(Sprite):

    def __init__(self,x,y,screen):#x，y是砖块的坐标
        super().__init__()
        self.screen = screen
        self.image = pygame.image.load('images/ground.bmp')
        self.image = pygame.transform.scale(self.image,(20,20))
        self.rect = self.image.get_rect()
        
        self.rect.x = x
        self.rect.y = y

    def drawblock(self):
        self.screen.blit(self.image, (self.rect.x,self.rect.y)) 

class Block_2(Sprite):
    def __init__(self,x,y,screen):
        super().__init__()
        self.screen = screen
        self.image = pygame.image.load('images/block.bmp')
        self.image = pygame.transform.scale(self.image,(60,20))
        self.rect = self.image.get_rect()
        
        self.rect.x = x
        self.rect.y = y

    def drawblock(self):
        self.screen.blit(self.image, (self.rect.x,self.rect.y))
           


