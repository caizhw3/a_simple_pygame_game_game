import pygame.font

class Button():
	def __init__(self,screen,msg,flag):
		self.screen = screen
		self.screen_rect = screen.get_rect()#///引入整个大屏幕的参数

		self.width = 200
		self.height = 50
		self.button_color = (0,255,0)
		self.text_color = (255,255,255)#//设置参数，在下面会调用，都是简单的数据类型而已。
		self.font = pygame.font.SysFont(None , 48)#//None是字体类型，48是字体大小

		self.rect = pygame.Rect(0 , 0 , self.width , self.height)#//设置矩形大小
		self.rect.center = self.screen_rect.center#//将矩形放到屏幕中间
		self.a = [100,0]
		self.rect.y -=self.a[flag]
		self.prep_msg(msg,flag)

	def prep_msg(self , msg,flag ):
		self.msg_image = self.font.render(msg , True , self.text_color , self.button_color)#//要打印的字符串，是否抗锯齿，字体颜色，矩形颜色
		self.msg_image_rect = self.msg_image.get_rect()#//不解释
		self.msg_image_rect.center = self.rect.center#//将该矩形放到中间去

	def draw_button(self):#//绘制的时候调用该函数
		self.screen.fill(self.button_color , self.rect)#//绘制出一个矩形作为背景
		self.screen.blit(self.msg_image,self.msg_image_rect)#//在一个矩形中绘制出文本信息

	