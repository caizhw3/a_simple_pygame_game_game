import sys
import pygame
from bullet import Bullet
import math
from monster import Monster_little
from monster import Monster_boss
import BGM

def check_events1(play_button , working):  
    for event in pygame.event.get():  
        if event.type == pygame.QUIT:  
            sys.exit()  
        elif event.type == pygame.MOUSEBUTTONDOWN:  
            mouse_x,mouse_y = pygame.mouse.get_pos()  
            model = check_play_button(working , play_button , mouse_x , mouse_y)  
            return model

def check_play_button(working ,play_button ,mouse_x , mouse_y):  
    if play_button[0].rect.collidepoint(mouse_x , mouse_y) and working [0] ==False :  
        working[0] = True    
        pygame.mouse.set_visible(False)  
        return 1
    elif play_button[1].rect.collidepoint(mouse_x , mouse_y) and working [0] ==False :  
        working[0] = True    
        pygame.mouse.set_visible(False)  
        return 2
    else:
        return 0; 

def check_events2(ai_settings, screen,hero2, bullets,np):
	if(ai_settings.time2!=0):
		ai_settings.time2+=1
		if(ai_settings.time2 == 200):
			ai_settings.time2 = 0
	for eventType,eventKey in np.dest_event_get():
		if(eventType == 2 ):
		#	print('*********')
		#	print('pygame.KEYDOWN!!!')
		#	print('**********')
			if eventKey == pygame.K_RIGHT:
				hero2.moving_right = True
				hero2.change_hero()
			elif eventKey == pygame.K_LEFT:
				hero2.moving_left = True
				hero2.change_hero()
			elif eventKey == pygame.K_UP:
				hero2.moving_up = True
			elif eventKey == pygame.K_SPACE:
				if ai_settings.time2==0:
					ai_settings.time2+=1
					fire_bullet(ai_settings, screen, hero2, bullets)
		elif(eventType == pygame.KEYUP):
			if eventKey == pygame.K_RIGHT:
				hero2.moving_right = False
			elif eventKey == pygame.K_LEFT:
				hero2.moving_left = False

def check_events(ai_settings,screen,ship,bullets):
	"""响应按键和鼠标事件"""
	if(ai_settings.time!=0):
		ai_settings.time+=1
		if(ai_settings.time == 200):
			ai_settings.time = 0
	pg_event_get = pygame.event.get()
	#print(pg_event_get)
	for event in pg_event_get:
		if event.type == pygame.QUIT:
			sys.exit()

		elif event.type == pygame.KEYDOWN:
			#print(1)
			check_keydown_events(event,ai_settings,screen,ship,bullets)
	 
		elif event.type == pygame.KEYUP:
			check_keyup_events(event,ship)
	return pg_event_get

def check_keyup_events(event,ship):
	"""响应松开"""
	if event.key == pygame.K_RIGHT:
		ship.moving_right = False
	elif event.key == pygame.K_LEFT:
		ship.moving_left = False
	#elif event.key == pygame.K_UP:
		#ship.moving_up = False

def check_keydown_events(event,ai_settings,screen,hero,bullets):
	"""响应按键"""
	if event.key == pygame.K_RIGHT:
		hero.moving_right = True
		hero.change_hero()
	elif event.key == pygame.K_LEFT:
		hero.moving_left = True
		hero.change_hero()
	elif event.key == pygame.K_UP:
		hero.moving_up = True
	elif event.key == pygame.K_SPACE:
		if ai_settings.time==0:
			ai_settings.time+=1
			
			fire_bullet(ai_settings, screen, hero, bullets)

def fire_bullet(ai_settings, screen, hero, bullets):
	if hero.direction :
		new_bullet=Bullet(ai_settings.bullet_width,ai_settings.bullet_height,ai_settings.bullet_speed_factor,screen,hero.rect.x-30,hero.rect.y+15,1)
	else :
		new_bullet=Bullet(ai_settings.bullet_width,ai_settings.bullet_height,ai_settings.bullet_speed_factor,screen,hero.rect.x+50,hero.rect.y+15,0)
	bullets.add(new_bullet)
	BGM.shoot_()##########################################################

def create_monster(n,monsters,ai_settings,screen,hero,bullets):
	if n[0] == 1:
		monster1=Monster_little(ai_settings,screen,hero,'images/怪兽1左.bmp',200,410,200,200)
		monster2=Monster_little(ai_settings,screen,hero,'images/怪兽1左.bmp',280,330,200,200)
		monster3=Monster_little(ai_settings,screen,hero,'images/怪兽1左.bmp',430,250,200,200)
		monster4=Monster_little(ai_settings,screen,hero,'images/怪兽1左.bmp',600,150,200,200)
		monster5=Monster_little(ai_settings,screen,hero,'images/怪兽1左.bmp',630,490,200,200)
		monster6=Monster_little(ai_settings,screen,hero,'images/怪兽1左.bmp',930,490,200,200)
		monsters.add(monster1)
		monsters.add(monster2)
		monsters.add(monster3)
		monsters.add(monster4)
		monsters.add(monster5)
		monsters.add(monster6)
	if n[0] == 2:
		ai_settings.monster_bullets_time = 2000
		monster1=Monster_little(ai_settings,screen,hero,'images/怪兽1左.bmp',180,490,200,200)
		ai_settings.monster_bullets_time = 400
		ai_settings.monster_hp = 75
		monster2=Monster_little(ai_settings,screen,hero,'images/怪兽2左.bmp',580,430,340,650)
		monster3=Monster_little(ai_settings,screen,hero,'images/怪兽2左.bmp',930,430,690,1000)
		ai_settings.monster_hp = 150
		ai_settings.monster_bullets_time = 400
		monster4=Monster_little(ai_settings,screen,hero,'images/怪兽2左.bmp',860,230,800,900)
		monsters.add(monster1)
		monsters.add(monster2)
		monsters.add(monster3)
		monsters.add(monster4)
	if n[0] == 3:
		ai_settings.monster_bullets_time = 400
		ai_settings.monster_hp = 500
		monster1=Monster_boss(ai_settings,screen,hero,'images/怪兽3左.bmp',580,390,340,650)
		monsters.add(monster1)
		ai_settings.monster_hp = 700
		ai_settings.monster_bullets_time = 300
		monster2=Monster_boss(ai_settings,screen,hero,'images/hero右.bmp',100,60,0,550)
		monsters.add(monster2)

def update_screen(ai_settings, screen, hero, sb, bullets,blocks,n,monsters,monster_bullets):
	# 每次循环时都重绘屏幕
	#if hero.hp_sum <=0:
	#	working[0] = False
	#	pygame.mouse.set_visible(True)  
	hero.blitme()
	#绘制显示板
	#sb.show_score()
	for bullet in bullets:
		bullet.update()
		bullet.draw_bullet()

	for bullet in monster_bullets:
		bullet.update()
		bullet.draw_bullet()
	
	for monster in monsters.copy():
		if monster.hp <=0:
			monsters.remove(monster)
		else:
			monster.blitme()
			if(monster.time != 0):
				monster.time+=1
			monster.shoot_hero(ai_settings ,hero,monster_bullets,screen)

	remove_bullet(bullets,blocks,monsters,ai_settings)
	remove_monster_bullet(monster_bullets,blocks,hero,ai_settings)
	hero_hit(hero,monsters,screen)

	if len(monsters) == 0:
		change_level(hero,n,screen)
	# 让最近绘制的屏幕可见
	pygame.display.flip()

def hero_hit(hero,monsters,screen):
	if pygame.sprite.spritecollide(hero,monsters,False) or hero.rect.bottom >= screen.get_rect().bottom:
		hero.hp_sum-=20
		hero.center = screen.get_rect().left + 25
		hero.bottom_ = screen.get_rect().bottom-40

def change_level(hero,n,screen):
	#if hero.
	if hero.rect.right >= screen.get_rect().right:
		n[0]+=1
		hero.center = screen.get_rect().left + 25
		hero.bottom_ = screen.get_rect().bottom-40
#colllisions = pygame.sprite.groupcollide(bullets , aliens , True , True )  

def remove_bullet(bullets,blocks,monsters,ai_settings):  
    for bullet in bullets.copy():  
        if bullet.rect.left >= 1000:  
            bullets.remove(bullet)  
        elif(bullet.rect.right<=0):
        	bullets.remove(bullet)
        else:
        	pygame.sprite.groupcollide(bullets ,blocks , True , False )
        	collisions = pygame.sprite.groupcollide(bullets ,monsters , True , False )
        	if collisions:
        		for i in collisions.values():
        			i[0].hp-=ai_settings.hero_bullets_power

def remove_monster_bullet(monster_bullets,blocks,hero,ai_settings):
	for bullet in monster_bullets.copy():
		if bullet.rect.left >= 1000:
			monster_bullets.remove(bullet)
		elif(bullet.rect.right<=0):
			monster_bullets.remove(bullet)
		else:
			pygame.sprite.groupcollide(monster_bullets ,blocks , True , False )
			collisions = pygame.sprite.spritecollide(hero,monster_bullets,True)
			if collisions:
				for bullet in collisions:
					hero.hp_sum -=ai_settings.monsters_bullets_power


def update_hero(ai_settings,screen,hero,blocks):
	"""人物跟场景发生接触时产生的变化"""
	hero.p_right = False
	hero.p_left = False
	collision = pygame.sprite.spritecollide(hero,blocks,False)
	
	if collision:
		for block in collision:
			if (math.fabs(hero.rect.bottom - block.rect.top)<=1) and (hero.rect.left-block.rect.right<=-2 and hero.rect.right-block.rect.left>=2):
				hero.set_stand()
				#print("ok")
				#print(hero.rect.left-block.rect.right)
			elif (math.fabs(hero.rect.left-block.rect.right)<=1) and (hero.rect.top-block.rect.bottom<=-2 and hero.rect.bottom-block.rect.top>=2):
				#print("123")
				hero.set_p_left()
				#hero.set_moving_up_false()
			elif (math.fabs(hero.rect.right-block.rect.left)<=1) and (hero.rect.top-block.rect.bottom<=-2 and hero.rect.bottom-block.rect.top>=2):
				hero.set_p_right()
				#hero.set_moving_up_false()
			elif (math.fabs(hero.rect.top-block.rect.bottom)<=1) and ((hero.rect.left-block.rect.right<=-2 and hero.rect.right-block.rect.left>=2)):
				hero.set_drop()
				hero.set_moving_up_false()
	else:
		if hero.moving_up == False:
			hero.set_drop()

def update_screen2(ai_settings, screen, hero1, sb1, hero2,sb2,bullets,blocks,n,monsters,monster_bullets):
	# 每次循环时都重绘屏幕
	hero1.blitme()
	hero2.blitme()
	#绘制显示板
	#sb1.show_score()
	#sb2.show_score()
	#print(sb[i].hp_sum)
		
	for bullet in bullets:
		bullet.update()
		bullet.draw_bullet()

	for bullet in monster_bullets:
		bullet.update()
		bullet.draw_bullet()
	
	for monster in monsters.copy():
		if monster.hp <=0:
			monsters.remove(monster)
		else:
			monster.blitme()
			if(monster.time != 0):
				monster.time+=1
			monster.shoot_hero(ai_settings ,hero1,monster_bullets,screen)
			monster.shoot_hero(ai_settings ,hero2,monster_bullets,screen)

	remove_bullet(bullets,blocks,monsters,ai_settings)
	remove_monster_bullet(monster_bullets,blocks,hero1,ai_settings)
	remove_monster_bullet(monster_bullets,blocks,hero2,ai_settings)
	hero_hit(hero1,monsters,screen)
	hero_hit(hero2,monsters,screen)

	if len(monsters) == 0:
		change_level2(hero1, hero2, n, screen)

	# 让最近绘制的屏幕可见
	pygame.display.flip()
def change_level2(hero1, hero2, n, screen):
	if hero1.rect.right >= screen.get_rect().right -15 or hero2.rect.right >= screen.get_rect().right -15:
		n[0]+=1
		hero1.center = screen.get_rect().left + 25
		hero1.bottom_ = screen.get_rect().bottom-40
		hero2.center = screen.get_rect().left + 25
		hero2.bottom_ = screen.get_rect().bottom-40
