import pygame.font 
import pygame
class Scoreboard():
	"""显示得分信息的类"""
	def __init__(self, ai_settings, screen, hero,i): 
		"""初始化显示得分涉及的属性"""
		self.screen = screen
		self.screen_rect = screen.get_rect() 
		self.ai_settings = ai_settings
		self.hero = hero
		self.i = i
		
		# 显示得分信息时使用的字体设置 
		self.text_color = (30, 30, 30)
		self.font = pygame.font.SysFont(None, 30)
		self.hp_sum = self.hero.hp_sum

		#血量槽
		self.hp_bg_color = 255,255,255
		self.hp_bg_image = pygame.Rect(20,40+45*i,200,20)
		
		#显示板
		self.hp_bg_color1 = 230, 230, 230
		self.hp_bg_image1 = pygame.Rect(20,40+45*i,50,20)
		
		self.hp_color = 255,0,0

		# 准备初始得分图像 
		self.show_score()
		
	def show_score(self):
		"""在屏幕上显示得分""" 
		
		#血量剩余多少
		
		self.hp_image =  pygame.Rect(20,40+45*self.i,self.hp_sum*2,20)
		'''
		#血量槽
		self.hp_bg_color = 255,255,255
		self.hp_bg_image = pygame.Rect(20,40,200,20)
		
		#显示板
		self.hp_bg_color1 = 230, 230, 230
		self.hp_bg_image1 = pygame.Rect(20,40,50,20)
		'''
		#将背景板绘制出来
		pygame.draw.rect(self.screen, self.hp_bg_color1, self.hp_bg_image1)
		#将血量条绘制出来
		pygame.draw.rect(self.screen, self.hp_bg_color, self.hp_bg_image)
		#将血槽绘制出来
		pygame.draw.rect(self.screen, self.hp_color, self.hp_image)
		
		score_str = "HP:"+ str(self.hp_sum) + "%"  

		self.score_image = self.font.render(score_str, True, self.text_color,
			self.hp_bg_color1)
		
		# 将血量百分比放在屏幕左上角
		self.score_rect = self.score_image.get_rect() 
		self.score_rect.left = self.screen_rect.left+20 
		self.score_rect.top = 20+45*self.i
		
		self.screen.blit(self.score_image, self.score_rect)
		
	def update_scoreboard(self,ai_settings, screen, hero):
		self.hp_sum = self.hero.hp_sum

	def died(self):
		self.hp_sum = 0 

