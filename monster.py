import pygame
from pygame.sprite import Sprite
from bullet import Bullet
#().__init__()
class Monster_little(Sprite): 
	def __init__(self,ai_settings,screen,hero,str_,x,y,move_x_left,move_x_right):
		super().__init__()
		self.screen = screen
		self.ai_settings=ai_settings
		self.time_shoot = ai_settings.monster_bullets_time
		self.speed = ai_settings.monster_lr_speed

		#加载怪兽图片
		self.image = pygame.image.load(str_)
		self.image = pygame.transform.scale(self.image,(70,70))
		self.hp = ai_settings.monster_hp
		self.moving_right = move_x_right
		self.moving_left = move_x_left
		self.direction = 1 #1 left  0 right 
		self.time = 0
		'''
		例如
		self.image = pygame.image.load('images/hero右.bmp')
		'''
		self.rect = self.image.get_rect()
		self.screen_rect = screen.get_rect()
		self.rect.x = x
		self.rect.y = y
		self.x = float(x)
		self.y = float(y)
		
		#如果与人物统一水平线，就停住
		self.stop = False	
	
	def set_stop(self):
		self.stop = True
		
	def set_turn(self):
		self.turn = True
		
	def update(self):
		if self.moving_right == self.moving_left:
			pass
		elif self.stop == True:
			self.stop =False
		elif self.direction == 1:
			self.x -=self.speed
			if self.rect.left <= self.moving_left:
				self.direction = 0
				self.image = pygame.image.load('images/怪兽2右 .bmp')
				self.image = pygame.transform.scale(self.image,(70,70))
		else :
			self.x += self.speed
			if self.rect.right >= self.moving_right:
				self.direction = 1 
				self.image = pygame.image.load('images/怪兽2左.bmp')
				self.image = pygame.transform.scale(self.image,(70,70))

		self.rect.x = self.x
		
		
	def shoot_hero(self,ai_settings ,hero,monster_bullets,screen):
		if self.time == self.time_shoot :
			self.time = 0
		line = (self.rect.top+self.rect.bottom)/2
		if self.direction == 1 :
			if hero.rect.top < line and hero.rect.bottom > line and hero.rect.right <self.rect.left:
				self.shoot(ai_settings,monster_bullets,screen)
				self.stop = True
		else :
			if hero.rect.top < line and hero.rect.bottom > line and hero.rect.left >self.rect.right:
				self.shoot(ai_settings,monster_bullets,screen)
				self.stop = True

	def shoot(self,ai_settings,monster_bullets,screen):
		if self.direction and self.time ==0:
			new_bullet=Bullet(ai_settings.bullet_width,ai_settings.bullet_height,ai_settings.bullet_speed_factor,screen,self.rect.x-30,self.rect.y+20,1)
			self.time =1
			monster_bullets.add(new_bullet)

		elif self.direction==0 and self.time ==0:
			new_bullet=Bullet(ai_settings.bullet_width,ai_settings.bullet_height,ai_settings.bullet_speed_factor,screen,self.rect.x+50,self.rect.y+20,0)
			self.time =1
			monster_bullets.add(new_bullet)
		
	def blitme(self):
		self.screen.blit(self.image,self.rect)
	
class Monster_boss(Sprite):	
	def __init__(self,ai_settings,screen,hero,str_,x,y,move_x_left,move_x_right):
		super().__init__()
		self.screen = screen
		self.ai_settings=ai_settings
		self.time_shoot = ai_settings.monster_bullets_time
		self.speed = ai_settings.monster_lr_speed

		#加载怪兽图片
		self.image = pygame.image.load(str_)
		self.image = pygame.transform.scale(self.image,(50,70))
		self.hp = ai_settings.monster_hp
		self.moving_right = move_x_right
		self.moving_left = move_x_left
		self.direction = 1 #1 left  0 right 
		self.time = 0
		'''
		例如
		self.image = pygame.image.load('images/hero右.bmp')
		'''
		self.rect = self.image.get_rect()
		self.screen_rect = screen.get_rect()
		self.rect.x = x
		self.rect.y = y
		self.x = float(x)
		self.y = float(y)
		
		#如果与人物统一水平线，就停住
		self.stop = False	
	
	def set_stop(self):
		self.stop = True
		
	def set_turn(self):
		self.turn = True
		
	def update(self):
		if self.moving_right == self.moving_left:
			pass
		elif self.stop == True:
			self.stop =False
		elif self.direction == 1:
			self.x -=self.speed
			if self.rect.left <= self.moving_left:
				self.direction = 0
		else :
			self.x += self.speed
			if self.rect.right >= self.moving_right:
				self.direction = 1 
		self.rect.x = self.x
		
		
	def shoot_hero(self,ai_settings ,hero,monster_bullets,screen):
		if self.time == self.time_shoot :
			self.time = 0
		line = (self.rect.top+self.rect.bottom)/2
		if hero.rect.top < line and hero.rect.bottom > line :
			if hero.rect.right <self.rect.left:
				self.direction = 1 
			else:
				self.direction = 0  
			self.shoot(ai_settings,monster_bullets,screen)
			self.stop = True

	def shoot(self,ai_settings,monster_bullets,screen):
		if self.time ==0:
			new_bullet=Bullet(ai_settings.bullet_width,ai_settings.bullet_height,ai_settings.bullet_speed_factor,screen,self.rect.x+50-80*self.direction,self.rect.y+23,self.direction)
			self.time =1
			monster_bullets.add(new_bullet)

	def blitme(self):
		self.screen.blit(self.image,self.rect)
		
		