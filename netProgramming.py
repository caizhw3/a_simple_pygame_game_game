from socket import *
from threading import Thread
import pygame
#1.设计一个网络编程类，里面封装了游戏双方需要传递消息需要的动作
class netPrograming():
#2.初始化
	def __init__(self,destIp = '',destPort = 0,hostPort = 0):
		#创建套接字，绑定端口，初始化属性
		self.udpSocket = socket(AF_INET,SOCK_DGRAM)
		self.udpSocket.bind(("",hostPort))
		self.destIp = destIp
		self.destPort = destPort
		self.hostPort = hostPort
		#下面是看其他搭档需要传递什么属性了
		#简单测试一下eventType 和 eventKey好了
		self.eventType = 0
		self.eventKey = 0
		self.destType = []
		self.destKey = []
		self.hero_centerx = 25
		self.hero_bottom = 560
		self.dest_hero_centerx = 25
		self.dest_hero_bottom = 560
		self.dest_have_start = False
	
	#调用这个函数来更新需要传递的内容,这里还不是很完整，需要和其他搭档进行沟通
	#暂时弃用吧。
	def update(self,eventType ,eventKey ):
		self.eventType = eventType
		self.eventKey = eventKey
		

#3收到消息，返回对方按键检测的值(string)，需要测试内容是什么属性，然后修改相关的属性
#再次修改版
	def recvData(self):
		if(self.dest_have_start == False):
			while self.dest_have_start == False:
				try:
					recvInfo = self.udpSocket.recvfrom(1024)
				except Exception as err:
					continue
				self.dest_have_start = True
		while True:
			recvInfo = self.udpSocket.recvfrom(1024)
			msg = recvInfo[0].decode('gb2312')

			msg_list = msg.split(':')

			msg_list_int = []
			msg_list_int.append(int(msg_list[0]))
			msg_list_int.append(int(msg_list[1]))
			msg_list_int.append(int(msg_list[2]))
			msg_list_int.append(int(msg_list[3]))

			self.destType.append(msg_list_int[0])
			self.destKey.append(msg_list_int[1])
			self.dest_hero_centerx = msg_list_int[2]
			self.dest_hero_bottom = msg_list_int[3]

#返回当前的收到所有内容，返回后清空，以元组方式返回
	def dest_event_get(self):
		event_get = []
		for i in range(0,len(self.destKey)):
			event_get.append((self.destType[i],self.destKey[i]))
		self.destType = []
		self.destKey = []
		return event_get
		
		
		
		
	def update_my_X_Y(self,hero):
		self.hero_centerx = hero.rect.centerx
		self.hero_bottom  = hero.rect.bottom
		self.eventType = 0
		self.eventKey = 0
		self.sendData()

#4.发送消息
	def  sendData(self):
		msg = str(str(self.eventType)+':'+str(self.eventKey)+':'+str(self.hero_centerx)+':'+str(self.hero_bottom))
		self.udpSocket.sendto(msg.encode('gb2312'),(self.destIp,self.destPort))
		#print('~~~~~~~~~')
		#print(msg)
		#print('~~~~~~~~~')
###################################################################
#将一个event里面的全部内容发过去
	def loop_sent_msg(self,pg_event_get,hero):
		self.hero_centerx = hero.rect.centerx
		self.hero_bottom  = hero.rect.bottom
		if len(pg_event_get) == 0:
			#print(-1111)
			self.eventType = 0
			self.eventKey = 0
			self.sendData()
			
		for event in pg_event_get:
			#print('++++++++++')
			self.eventType = event.type
			#print('event.type ='+str(event.type))
			if(self.eventType == pygame.KEYDOWN or self.eventType == pygame.KEYUP):
				self.eventKey = event.key
				#self.sendData()
				#print('event.key  ='+str(event.key))
			#print('++++++++++')
			self.sendData()

'''
#创建一个测试函数		
def multi_thread():
	destIp = '192.168.146.133'
	destPort = 3411
	hostPort = 3411
	np = netPrograming(destIp,destPort,hostPort)
	np.dest_event_get()
	tr = Thread(target = np.recvData)
	ts = Thread(target = np.sendData)
	
	tr.start()
	ts.start()
	
	tr.join()
	ts.join()
	
multi_thread()

'''
